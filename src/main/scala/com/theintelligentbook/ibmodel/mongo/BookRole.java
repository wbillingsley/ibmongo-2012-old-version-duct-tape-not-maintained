package com.theintelligentbook.ibmodel.mongo;

/**
 * Created by IntelliJ IDEA.
 * User: wbillingsley
 * Date: 5/04/11
 * Time: 4:54 PM
 * To change this template use File | Settings | File Templates.
 */
public enum BookRole {

    Reader,
    Chatter,
    Statistician,
    Tutor,
    Moderator,
    Administrator,
    Author,
    Owner

}
