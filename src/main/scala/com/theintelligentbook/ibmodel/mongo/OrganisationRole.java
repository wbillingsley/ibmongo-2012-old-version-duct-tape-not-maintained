package com.theintelligentbook.ibmodel.mongo;

/**
 * Created by IntelliJ IDEA.
 * User: wbillingsley
 * Date: 18/04/11
 * Time: 10:13 PM
 * To change this template use File | Settings | File Templates.
 */
public enum OrganisationRole {

    Member,
    Student,
    Author,
    Moderator,
    Administrator,
    Owner

}
