package com.theintelligentbook.ibmodel.mongo

import Imports._
import com.wbillingsley.handy._
import com.mongodb.casbah.Imports._
import com.novus.salat._
import com.novus.salat.global._
import com.novus.salat.dao.SalatDAO
import java.util.Date
import com.novus.salat.annotations._

/**
 * A votable question in a Q&A forum
 */
case class QnAAnswer(

  _id:ObjectId = new ObjectId(),
  
  @Key("addedBy") _addedBy:Option[ObjectId] = None,

  val session:Option[String] = None,

  var text:String,
  
  var score:Int = 0,
  
  var votes:Seq[Vote] = Seq.empty,
  
  var views:Int = 0,  
  
  var comments:Seq[EmbeddedComment] = Seq.empty,
  
  val created:Date = new Date,
  
  var accepted:Boolean = false,
  
  var updated:Date = new Date

) extends HasObjectId {
  
  def id = _id
   
  def addedBy = Ref.fromOptionId(classOf[Reader], _addedBy)
  
}

/*

object QnAAnswerDAO extends AbstractDAO[QnAAnswer]("qnaAnswer") {  

  def newAnswer(q:Ref[QnAQuestion], addedBy:Ref[Reader], session:Option[String], text:String) = {
    new QnAAnswer(_question = q.getId, _addedBy = addedBy.getId, session = session, text = Some(text))
  }
  
  def findNewest(q:Ref[QnAQuestion], after:Ref[QnAAnswer] = None, limit:Int = 50) = {
    val opt = for (qid <- q.getId) yield {
      val query = after.getId match {
	        case Some(aid) => sdao.find(MongoDBObject("question" -> qid) ++ ("_id" $gt aid)) 
	        case None => sdao.find(MongoDBObject("question" -> qid))
	      }
	  query.limit(limit)
	  new RefTraversableOnce(query.toSeq)	      
    }
    opt.getOrElse(RefNone)
  }  
  
  def save(e:QnAAnswer) {
    DAO.cache(RefById(classOf[QnAAnswer], e.id), RefItself(e))
    sdao.save(e)
  }

} */
