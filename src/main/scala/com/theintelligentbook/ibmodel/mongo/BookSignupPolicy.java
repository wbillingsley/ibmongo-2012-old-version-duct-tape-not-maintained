package com.theintelligentbook.ibmodel.mongo;

/**
 * Created by IntelliJ IDEA.
 * User: wbillingsley
 * Date: 28/04/11
 * Time: 9:28 PM
 * To change this template use File | Settings | File Templates.
 */
public enum BookSignupPolicy {
    open,
    loggedIn,
    organisation,
    invite,
    silver,
    gold
}
