/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.theintelligentbook.ibmodel.mongo

import com.wbillingsley.handy._
import com.mongodb.casbah.Imports._
import java.util.Date
import com.novus.salat._
import com.novus.salat.global._
import com.novus.salat.dao.SalatDAO
import com.novus.salat.annotations._

case class Poll(
  _id:ObjectId = new ObjectId(),

  @Key("book") _book:Option[ObjectId] = None,

  @Key("ce") _ce:Option[ObjectId] = None,

  @Key("createdBy") _createdBy:Option[ObjectId] = None,

  var question:Option[String] = None,

  var anonymous:Boolean = true,

  var options:Seq[String] = Seq.empty[String],

  var optFeedback:Seq[String] = Seq.empty[String],

  var optScore:Seq[Int] = Seq.empty[Int],

  @Key("mode") var _mode:String = PollMode.chooseOne.toString,

  @Key("answerVis") var _answerVis:String = PollResultsVisibility.secret.toString,

  @Key("resultsVis") var _resultsVis:String = PollResultsVisibility.visible.toString,

  var mayMoveVote:Boolean = true,

  created : Date = new Date()
) extends HasObjectId  {

  def id = _id

  def ce = Ref.fromOptionId(classOf[ContentEntry], _ce)

  def book = Ref.fromOptionId(classOf[Book], _book)

  def createdBy = Ref.fromOptionId(classOf[Reader], _createdBy)

  def mode = PollMode.valueOf(_mode)
  def mode_=(v:PollMode) = _mode_=(v.toString)

  def answerVisibility = PollResultsVisibility.valueOf(_answerVis)
  def answerVisibility_=(v:PollResultsVisibility) = _answerVis_=(v.toString)

  def resultsVisibility = PollResultsVisibility.valueOf(_resultsVis)
  def resultsVisibility_=(v:PollResultsVisibility) = _resultsVis_=(v.toString)

  def protect = ce.toOption.map(_.protect).getOrElse(false)
  
}

object PollDAO extends AbstractDAO[Poll]("poll") {

  def newPoll(b:Ref[Book], ce:Ref[ContentEntry], r:Ref[Reader]) = {
    val p = new Poll(
      _book = b.getId,
      _ce = ce.getId,
      _createdBy = r.getId
    )
    DAO.cache(RefById(classOf[Poll], p.id), RefItself(p))
    p
  }

  def save(p:Poll) {
    DAO.cache(RefById(classOf[Poll], p.id), RefItself(p))
    ContentEntryDAO.touch(p.ce)
    sdao.save(p)
  }

}
