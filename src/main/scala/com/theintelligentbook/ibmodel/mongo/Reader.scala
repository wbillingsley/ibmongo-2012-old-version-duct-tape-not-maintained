package com.theintelligentbook.ibmodel.mongo

import _root_.java.util.Date
import com.wbillingsley.handy._

import com.mongodb.casbah.Imports._
import java.util.Date
import com.novus.salat._
import com.novus.salat.global._
import com.novus.salat.dao.SalatDAO
import scala.collection.mutable


case class Reader  (

  _id:ObjectId = new ObjectId(),

  var nickname:Option[String] = None,

  var fullname:Option[String] = None,

  var email:Option[String] = None,

  var _siteRoles:Set[String] = Set(
    SiteRole.Reader.toString,
    SiteRole.Author.toString
  )

) extends HasObjectId {
  
  def id = _id
  
  def registrations = RegistrationDAO.findByReader(_id)
  
  def siteRoles = _siteRoles.map(r => SiteRole.valueOf(r))
  def siteRoles_=(roles:Set[SiteRole]) {
    _siteRoles = roles.map {_.toString}
  }
  def addSiteRole(r:SiteRole) {
    _siteRoles += r.toString
  }  
  
  def save() = ReaderDAO.save(this)
  
}


object ReaderDAO extends AbstractDAO[Reader]("reader") {

  def newReader = {
    val r = new Reader
    DAO.cache(RefById(classOf[Reader], r.id), RefItself(r))
    r
  }

  def save(r:Reader) {
    DAO.cache(RefById(classOf[Reader], r.id), RefItself(r))
    sdao.save(r)
  }

  /**
   * True if no @link{Reader}s have yet been registered; false if there are readers or if the query fails.
   */
  def noReaders:Boolean = {
    sdao.find(MongoDBObject()).length <= 0
  }

}
