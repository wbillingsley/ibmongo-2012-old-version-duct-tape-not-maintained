package com.theintelligentbook.ibmodel.mongo

import Imports._
import com.wbillingsley.handy._
import com.mongodb.casbah.Imports._
import com.novus.salat._
import com.novus.salat.global._
import com.novus.salat.dao.SalatDAO
import java.util.Date
import com.novus.salat.annotations._

/**
 * Created with IntelliJ IDEA.
 * User: wbillingsley
 * Date: 22/08/12
 * Time: 1:08 AM
 * To change this template use File | Settings | File Templates.
 */
case class EntryComment(

  _id:ObjectId = new ObjectId(),

  @Key("ce") _ce:Option[ObjectId] = None,

  @Key("book") _book:Option[ObjectId] = None,
  
  @Key("reader") _reader:Option[ObjectId] = None,

  val session:Option[String] = None,

  val comment:Option[String] = None,

  val time:Date = new Date

) extends HasObjectId {
  
  def id = _id
  
  def ce = Ref.fromOptionId(classOf[ContentEntry], _ce)
  
  def book = Ref.fromOptionId(classOf[Book], _book)

  def reader = Ref.fromOptionId(classOf[Reader], _reader)
  
}


object EntryCommentDAO extends AbstractDAO[EntryComment]("entryComment") {
  
  def findByCE(ce:Ref[ContentEntry], skip:Int = 0, limit:Int = 20):Cursor[EntryComment] = {
    val id = ce.getId
    val cursor = sdao.find(MongoDBObject("ce" -> id)).sort(MongoDBObject("_id" -> 1))
    cursor.limit(limit)
    if (skip < 0) {
      val skipToLast = ((cursor.count - 1) / limit) * limit
      cursor.skip(skipToLast)
    } else {
      cursor.skip(skip)
    }
    
    cursor    
  }

  def findByBook(book:Ref[Book], skip:Option[Int] = None, limit:Option[Int] = None):Cursor[EntryComment] = {
    val id = book.getId
    val cursor = sdao.find(MongoDBObject("book" -> id)).sort(MongoDBObject("_id" -> 1))
    limit.foreach(cursor.limit(_))
    skip.foreach(cursor.skip(_))
    cursor    
  }
  
  def newEntryComment(
    ce:Ref[ContentEntry] = RefNone,
    book:Ref[Book] = RefNone,
    reader:Ref[Reader] = RefNone,
    session:Option[String] = None,
    comment:String
  ):EntryComment = {
    val e = new EntryComment(
      _book = book.getId, _ce = ce.getId, _reader = reader.getId,
      session=session, comment=Some(comment)
    )
    DAO.cache(RefById(classOf[EntryComment], e.id), RefItself(e))
    e
  }

  def save(e:EntryComment) {
    DAO.cache(RefById(classOf[LikeEntry], e.id), RefItself(e))
    sdao.save(e)
  }

}
