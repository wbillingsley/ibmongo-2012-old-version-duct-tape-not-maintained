package com.theintelligentbook.ibmodel.mongo

import com.mongodb.casbah.Imports._
import java.util.Date
import com.novus.salat._
import com.novus.salat.global._
import com.novus.salat.dao.SalatDAO
import com.mongodb.casbah.gridfs.Imports._
import java.io.InputStream
import com.wbillingsley.handy.{RefItself, RefById, Ref}
import com.novus.salat.annotations._

case class MediaFile(

  _id:ObjectId = new ObjectId(),

  @Key("book") _book:Option[ObjectId] = None,

  @Key("addedBy") _addedBy:Option[ObjectId] = None,

  mimeType:Option[String] = None,

  extension:Option[String] = None,

  var name:Option[String] = None,

  var shortDesc:Option[String] = None,

  var public:Boolean = false,

  path:String = "",

  created:Date = new Date()

) extends HasObjectId {

  def id = _id

  def book = Ref.fromOptionId(classOf[Book], _book)

  def addedBy = Ref.fromOptionId(classOf[Reader], _addedBy)

}

object MediaFileDAO extends AbstractDAO[MediaFile]("mediaFile") {

  private def newPath(name:Option[String], extn:Option[String]) = {
    val sb = new StringBuilder
    name.map(n => sb.append(n))
    sb.append((new ObjectId).toString)
    extn.map(e => {
      sb.append(".")
      sb.append(e)
    })
    sb.toString
  }

  def newMediaFile(
    book:Ref[Book],
    addedBy:Ref[Reader],
    mimeType:Option[String],
    extension:Option[String] = None,
    name:Option[String] = None,
    shortDescription:Option[String] = None
  ) = {
    val m = new MediaFile(
      _book = book.getId,
      _addedBy = addedBy.getId,
      mimeType = mimeType,
      extension = extension,
      name = name,
      shortDesc = shortDescription,
      path = newPath(name, extension)
    )
    DAO.cache(RefById(classOf[MediaFile], m.id), RefItself(m))
    m
  }

  def save(m:MediaFile) {
    DAO.cache(RefById(classOf[MediaFile], m.id), RefItself(m))
    sdao.save(m)
  }

  def saveStream(m:MediaFile, i:InputStream) = {
    val gridfs = GridFS(DAO.getDB())
    gridfs(i) { fh =>
      fh.filename = m.path
      m.mimeType.foreach(fh.contentType = _)
    }
  }

  def getStream(m:MediaFile) = {
    val gridfs = GridFS(DAO.getDB())
    gridfs.findOne(m.path).map(_.inputStream)
  }

}
