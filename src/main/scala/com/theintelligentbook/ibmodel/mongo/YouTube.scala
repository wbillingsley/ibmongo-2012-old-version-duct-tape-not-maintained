package com.theintelligentbook.ibmodel.mongo

import com.mongodb.casbah.Imports._
import com.novus.salat._
import com.novus.salat.global._
import com.novus.salat.dao.SalatDAO
import com.wbillingsley.handy.{RefItself, RefNone, RefById, Ref}
import com.novus.salat.annotations._

case class YouTube(
  _id:ObjectId = new ObjectId(),

  @Key("ce") _ce:Option[ObjectId] = None,

  @Key("book") _book:Option[ObjectId] = None,

  var youTubeId:Option[String] = None
) extends HasObjectId {
  
  def id = _id
  
  def ce = Ref.fromOptionId(classOf[ContentEntry], _ce)
  
  def book = Ref.fromOptionId(classOf[Book], _book)
}

object YouTubeDAO extends AbstractDAO[YouTube]("youTube") {

  def newYouTube(book:Ref[Book], ce:Ref[ContentEntry], youTubeId:Option[String]) = {
    val cc = new YouTube(
      _book = book.getId,
      _ce=ce.getId,
      youTubeId=youTubeId
    )
    DAO.cache(RefById(classOf[YouTube], cc.id), RefItself(cc))
    cc
  }

  def save(p:YouTube) {
    DAO.cache(RefById(classOf[YouTube], p.id), RefItself(p))
    ContentEntryDAO.touch(p.ce)
    sdao.save(p)
  }

}

