package com.theintelligentbook.ibmodel.mongo

import Imports._
import com.wbillingsley.handy._
import com.mongodb.casbah.Imports._
import com.novus.salat._
import com.novus.salat.global._
import com.novus.salat.dao.SalatDAO
import java.util.Date
import com.novus.salat.annotations._

/**
 * A votable question in a Q&A forum
 */
case class QnAQuestion(

  _id:ObjectId = new ObjectId(),

  @Key("book") _book:Option[ObjectId] = None,
  
  @Key("addedBy") _addedBy:Option[ObjectId] = None,

  val session:Option[String] = None,
  
  var title:String = "",

  var body:String = "",
  
  var score:Int = 0,
  
  var votes:Seq[Vote] = Seq.empty,
  
  var views:Int = 0,
  
  var tags:Set[String] = Set.empty,
  
  var commentCount:Int = 0,

  var comments:Seq[EmbeddedComment] = Seq.empty,
    
  var answerCount:Int = 0,
  
  var answers:Seq[QnAAnswer] = Seq.empty,
  
  var answerAccepted:Boolean = false,

  val created:Date = new Date,  
  
  var updated:Date = new Date
  

) extends HasObjectId {
  
  def id = _id
   
  def book = Ref.fromOptionId(classOf[Book], _book)

  def addedBy = Ref.fromOptionId(classOf[Reader], _addedBy)
  
}


object QnAQuestionDAO extends AbstractDAO[QnAQuestion]("qnaQuestion") {  
  
  def newQuestion(b:Ref[Book], addedBy:Ref[Reader], session:Option[String], title:String, body:String) = {
    new QnAQuestion(_book = b.getId, _addedBy = addedBy.getId, session = session, title=title, body=body)
  }  
  
  def incrementViewed(q:Ref[QnAQuestion]) = {
    for (qId <- q.getId) {
      val update = MongoDBObject("$inc" -> MongoDBObject("views" -> 1))
      val query = MongoDBObject("_id" -> qId)
      sdao.update(query, update)      
    }
  }
  
  def addAnswer(q:Ref[QnAQuestion], addedBy:Ref[Reader], session:Option[String], body:String) {
    for (qId <- q.getId) {
      val ans = new QnAAnswer(_addedBy = addedBy.getId, session = session, text = body)
      val dbAns = grater[QnAAnswer].asDBObject(ans)            
      val update = MongoDBObject("$push" -> MongoDBObject("answers" -> dbAns), "$inc" -> MongoDBObject("answerCount" -> 1), "$set" -> MongoDBObject("updated" -> new Date))
      val query = MongoDBObject("_id" -> qId)
      sdao.update(query, update)
    }
  }

  def addQComment(q:Ref[QnAQuestion], addedBy:Ref[Reader], text:String) {
    for (qId <- q.getId) {
      val c = new EmbeddedComment(_addedBy = addedBy.getId, text=text)
      val dbC = grater[EmbeddedComment].asDBObject(c)            
      val update = MongoDBObject("$push" -> MongoDBObject("comments" -> dbC), "$inc" -> MongoDBObject("commentCount" -> 1), "$set" -> MongoDBObject("updated" -> new Date))
      val query = MongoDBObject("_id" -> qId)
      sdao.update(query, update)
    }
  }  

  def addAnsComment(q:Ref[QnAQuestion], a:Ref[QnAAnswer], addedBy:Ref[Reader], text:String) {
    for (qId <- q.getId; aId <- a.getId) {
      val c = new EmbeddedComment(_addedBy = addedBy.getId, text=text)
      val dbC = grater[EmbeddedComment].asDBObject(c)            
      val update = MongoDBObject("$push" -> MongoDBObject("answers.$.comments" -> dbC), "$inc" -> MongoDBObject("commentCount" -> 1), "$set" -> MongoDBObject("updated" -> new Date))
      val query = MongoDBObject("_id" -> qId, "answers._id" -> aId)
      sdao.update(query, update)
    }
  }  
  
  
  def findNewest(book:Ref[Book], skip:Option[Int] = None, limit:Int = 50) = {
    book.getId match {
      case Some(bid) => { 
	      val query = sdao.find(MongoDBObject("book" -> bid))
	      for (s <- skip) query.skip(s)
	      query.limit(limit)
	      new RefTraversableOnce(query.toSeq)
      }
      case None => RefNone
    }
  }
  
  def save(e:QnAQuestion) {
    DAO.cache(RefById(classOf[QnAQuestion], e.id), RefItself(e))
    sdao.save(e)
  }

}
