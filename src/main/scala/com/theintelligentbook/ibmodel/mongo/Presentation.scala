package com.theintelligentbook.ibmodel.mongo

import com.mongodb.casbah.Imports._
import java.util.Date
import com.novus.salat._
import com.novus.salat.global._
import com.novus.salat.dao.SalatDAO
import com.wbillingsley.handy._
import com.novus.salat.annotations._

/**
 *
 * @param _id
 * @param ce
 * @param book A presentation always belongs to a book
 * @param protect A presentation always has an associated ContentEntry (to record the name, topics, etc)
 * @param entries
 */
case class Presentation (

  _id:ObjectId = new ObjectId(),

  @Key("ce") _ce:Option[ObjectId] = None,

  @Key("book") _book:Option[ObjectId] = None,

  var protect:Boolean = false,

  @Key("entries") var _entries:Seq[ObjectId] = Seq.empty[ObjectId]
) extends HasObjectId  {

  def id = _id

  def book = Ref.fromOptionId(classOf[Book], _book)

  def ce = Ref.fromOptionId(classOf[ContentEntry], _ce)
   
  def entries = new RefManyById(classOf[ContentEntry], _entries)

  def entries_=(l:RefManyById[ContentEntry, _]) {
    _entries = l.getIds
  }
  
  def start = entries.first

}


object PresentationDAO extends AbstractDAO[Presentation]("presentation") {

  def presentationsContainingEntry(entry:Ref[ContentEntry]) = {
    val idOpt = entry.getId
    val result = idOpt map { oid =>
      val ccSeq = sdao.find(MongoDBObject("entries" -> oid))
      ccSeq.toSeq
    }
    result.getOrElse(Seq.empty[Presentation])
  }

  def newPresentation(book:Ref[Book], ce:Ref[ContentEntry]) = {
    val p = new Presentation(_book = book.getId, _ce = ce.getId)
    DAO.cache(RefById(classOf[Presentation], p.id), RefItself(p))
    p
  }

  def save(p:Presentation) {
    DAO.cache(RefById(classOf[Presentation], p.id), RefItself(p))
    sdao.save(p)
  }

}