package com.theintelligentbook.ibmodel.mongo

import com.mongodb.casbah.Imports._
import com.novus.salat._
import com.novus.salat.global._
import com.novus.salat.dao.SalatDAO
import com.wbillingsley.handy.{RefItself, RefNone, RefById, Ref}
import com.novus.salat.annotations._

case class GoogleSlides(
  _id:ObjectId = new ObjectId(),

  @Key("ce") _ce:Option[ObjectId] = None,

  @Key("book") _book:Option[ObjectId] = None,
  
  var embedCode:Option[String] = None,

  var presId:Option[String] = None
) extends HasObjectId {
  
  def id = _id
  
  def ce = Ref.fromOptionId(classOf[ContentEntry], _ce)
  
  def book = Ref.fromOptionId(classOf[Book], _book)
}

object GoogleSlidesDAO extends AbstractDAO[GoogleSlides]("googleSlides") {

  def newGooglePres(book:Ref[Book], ce:Ref[ContentEntry], embedCode:Option[String], presId:Option[String]) = {
    val cc = new GoogleSlides(
      _book = book.getId,
      _ce=ce.getId,
      embedCode=embedCode,
      presId=presId
    )
    DAO.cache(RefById(classOf[GoogleSlides], cc.id), RefItself(cc))
    cc
  }

  def save(p:GoogleSlides) {
    DAO.cache(RefById(classOf[GoogleSlides], p.id), RefItself(p))
    ContentEntryDAO.touch(p.ce)
    sdao.save(p)
  }

}

