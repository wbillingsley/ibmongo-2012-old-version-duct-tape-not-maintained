package com.theintelligentbook.ibmodel.mongo

object Imports {

    
  type Countable = {
    def count: Int
  }
  
  type Cursor[+A] = Iterator[A] with Countable 
  
}