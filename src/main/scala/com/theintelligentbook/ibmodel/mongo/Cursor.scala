package com.theintelligentbook.ibmodel.mongo

import com.novus.salat.dao.SalatMongoCursor

/*

 * This might or might not be a good idea. It's a cursor type that can be
 * across the result of a function on a database cursor, rather than just
 * a database cursor directly.
 * 
 * We do it this way rather than just as a structural type so that we can
 * make the map function return our type rather than just an Iterator that
 * would lose the count and skip. 
 *
abstract class Cursor[T] {

  def count:Int   
  
  def iterator:Iterator[T]
  
  def map[B](f: T => B):Cursor[B]
  
}

 * Decorates a Salat cursor
 *
class SalatCursor[T <: AnyRef](c: SalatMongoCursor[T]) extends Cursor[T] {
  
  def count = c.count
  
  def iterator = c
  
  def map[B](f: T => B):Cursor[B] = new IteratorCursor(c.map(f), count)  
  
}

class IteratorCursor[T](val iterator: Iterator[T], val count:Int) extends Cursor[T] {

  def map[B](f: T => B):Cursor[B] = new IteratorCursor(iterator.map(f), count)

}

object Cursor {
  
  def apply[T <: AnyRef](c: SalatMongoCursor[T]) = new SalatCursor(c)
  
}

*/

class EmptyCursor[T] extends Iterator[T] {
  
  def count = 0
  
  def hasNext = false
  
  def next = throw new IllegalStateException("Next called on an EmptyCursor")  
  
}