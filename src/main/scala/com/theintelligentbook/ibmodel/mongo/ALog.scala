package com.theintelligentbook.ibmodel.mongo

import com.mongodb.casbah.Imports._
import java.util.Date
import com.novus.salat._
import com.novus.salat.global._
import com.novus.salat.dao.SalatDAO
import com.wbillingsley.handy._
import com.novus.salat.annotations._

import com.mongodb.util.JSON


/**
 * An invitation allowing someone to register for an Intelligent Book
 */
case class ALogE(

  _id:ObjectId = new ObjectId(),

  @Key("ce") _ce:Option[ObjectId] = None,

  @Key("book") _book:Option[ObjectId] = None,

  @Key("reader") _reader:Option[ObjectId] = None,

  session:Option[String] = None,

  action:Option[String] = None,

  kind:Option[String] = None,

  site:Option[String] = None,

  adjs:Set[String] = Set.empty[String],

  nouns:Set[String] = Set.empty[String],

  topics:Set[String] = Set.empty[String],

  extra:Option[String] = None,

  time:Date = new Date
) extends HasObjectId {

  def id = _id

  def book = Ref.fromOptionId(classOf[Book], _book)

  def ce = Ref.fromOptionId(classOf[ContentEntry], _ce)

  def reader = Ref.fromOptionId(classOf[Reader], _reader)

}

object ALogEDAO extends AbstractDAO[ALogE]("alog") {

  def newALogE(
    ce:Ref[ContentEntry] = RefNone,
    book:Ref[Book] = RefNone,
    reader:Ref[Reader] = RefNone,
    session:Option[String] = None,
    action:Option[String] = None,
    kind:Option[String] = None,
    site:Option[String] = None,
    adjs:Set[String] = Set.empty[String],
    nouns:Set[String] = Set.empty[String],
    topics:Set[String] = Set.empty[String],
    extra:Option[String] = None
  ):ALogE = {
    val e = new ALogE(
      _book = book.getId, _ce = ce.getId, _reader = reader.getId,
      session=session, action=action, kind=kind, site=site,
      adjs=adjs, nouns=nouns, topics=topics,
      extra=extra
    )
    DAO.cache(RefById(classOf[ALogE], e.id), RefItself(e))
    e
  }

  def save(e:ALogE) {
    DAO.cache(RefById(classOf[ALogE], e.id), RefItself(e))
    sdao.save(e)
  }
  
  def updateStatsByDay(b:Ref[Book]) = {
    
    val mapJs = """
      function m() {
      	var t = this.time
        t.setMinutes(0)
        t.setSeconds(0)
        t.setMilliseconds(0)
    	var v = {
    	  book: this.book,
          time: t,
          readers: {},
          sessions: {},
    	  anonymous: 0,
          total: 1,
          kinds: {},
          sites: {},
          adjs: {},
          nouns: {},
          topics: {}
    	}
      
        if (this.reader) { v.readers[this.reader] = 1 } else { v.anonymous = 1 }
        if (this.session) { v.sessions[this.session] = 1 }
        if (this.kind) { v.kinds[this.kind] = 1 }
        if (this.site) { v.sites[this.site] = 1 }
        if (this.adjs) { this.adjs.forEach(function(adj) { v.adjs[adj] = 1 }) }
        if (this.nouns) { this.nouns.forEach(function(noun) { v.nouns[noun] = 1 }) }
        if (this.topics) { this.topics.forEach(function(topic) { v.topics[topic] = 1 }) }

    	emit( { book: this.book, time: t }, v )
      }
	"""
      
    val reduceJs = """
      function r(k, values) {
      	var n = { book: k.book, time: k.time, readers: {}, anonymous: 0, total: 0, sessions: {}, kinds: {}, sites: {}, adjs: {}, nouns: {}, topics: {}, ces: {} }
    	values.forEach(function(value) {
    		n.total += value.total
      		n.anonymous += value.anonymous
      		
      		function accrue(from, into) {
    			for (var k in from) {
      				into[k] = (into[k] || 0) + from[k]
    			}
    		}
      
      		accrue(value.readers, n.readers) 
      		accrue(value.sessions, n.sessions) 
      		accrue(value.kinds, n.kinds) 
      		accrue(value.sites, n.sites) 
    		accrue(value.adjs, n.adjs) 
      		accrue(value.nouns, n.nouns) 
      		accrue(value.topics, n.topics) 
        })
    	
        return n
      }
    """
      
      
    val bookId = b.getId.getOrElse(new ObjectId())
    
    val result = sdao.collection.mapReduce(mapJs, reduceJs, MapReduceMergeOutput("aggEvents"), Some(Map("book" -> bookId)), sort=None, limit=None, finalizeFunction=None, jsScope=None, verbose=false)
    result.cursor
  }

  def aggEvents(b:Ref[Book]) = {    
    DAO.getCollection("aggEvents").find(Map("_id.book" -> b.getId.getOrElse(new ObjectId())))
  }
  
}


case class AggEventCC(
	value:AggEventVal
)

/**
 * An Aggregated Events entry
 */
case class AggEventVal(
    
	book:Option[ObjectId] = None,
	
    time:Date = new Date,
    
    sessions:Map[String, Int] = Map.empty[String, Int],
    
    readers:Map[String, Int] = Map.empty[String, Int],
    
    anonymous:Int = 0,
    
    actions:Map[String, Int] = Map.empty[String, Int],
    
    kinds:Map[String, Int] = Map.empty[String, Int],
	
    sites:Map[String, Int] = Map.empty[String, Int],
    
    adjs:Map[String, Int] = Map.empty[String, Int],
    
    nouns:Map[String, Int] = Map.empty[String, Int],
    
    topics:Map[String, Int] = Map.empty[String, Int],
    
    ces:Map[String, Int] = Map.empty[String, Int]
)


object AggEventsDAO {

  val collName = "aggEvents"

  lazy val sdao = new SalatDAO[AggEventCC, ObjectId](DAO.getCollection(collName)) {
  }

  def aggEventsCC(b:Ref[Book]) = {
    sdao.find(Map("_id.book" -> b.getId))
  }
  
  def aggEventsJson(b:Ref[Book]):Iterator[String] = {    
    val c = DAO.getCollection("aggEvents").find(Map("_id.book" -> b.getId))
    c.map(o => JSON.serialize(o.get("value")))
  }
}
