package com.theintelligentbook.ibmodel.mongo

import com.wbillingsley.handy._
import com.mongodb.casbah.Imports._
import java.util.Date
import com.novus.salat._
import com.novus.salat.global._
import com.novus.salat.dao.SalatDAO
import com.novus.salat.annotations._

case class Membership(

  var _id:ObjectId = new ObjectId(),

  @Key("roles") var _roles:Set[String] = Set(OrganisationRole.Member.toString),

  @Key("reader") var _reader:Option[ObjectId] = None,

  @Key("org") var _org:Option[ObjectId] = None

) extends HasId[ObjectId] {

  def id = _id

  def roles = _roles.map {OrganisationRole.valueOf(_)}
  def roles_=(s:Set[OrganisationRole]) = _roles_=(s.map(_.toString))
  def addRole(r:OrganisationRole) {
    _roles += r.toString
  }

  def reader = Ref.fromOptionId(classOf[Reader], _reader)
  def reader_=(r:Ref[Reader]) = _reader_=(r.getId.asInstanceOf[Option[ObjectId]])

  def organisation = Ref.fromOptionId(classOf[Organisation], _org)
  def organisation_=(r:Ref[Organisation]) = _org_=(r.getId.asInstanceOf[Option[ObjectId]])

}

object MembershipDAO extends AbstractDAO[Membership]("membership") {

  def newMembership(o:Ref[Organisation], r:Ref[Reader], roles:Set[OrganisationRole]) = {
    val reg = new Membership
    reg.reader = r
    reg.organisation = o
    reg.roles = roles
    DAO.cache(RefById(classOf[Membership], reg.id), RefItself(reg))
    reg
  }

  def save(r:Membership) {
    DAO.cache(RefById(classOf[Membership], r.id), RefItself(r))
    sdao.save(r)
  }

  def findByReader(rid:ObjectId) = {
    val cursor = sdao.find(MongoDBObject("reader" -> rid))
    cursor.toSeq
  }

  def findByReaderAndBook(rid:ObjectId, bid:ObjectId) = {
    val cursor = sdao.find(MongoDBObject("reader" -> rid, "book" -> bid))
    cursor.toSeq
  }

}
