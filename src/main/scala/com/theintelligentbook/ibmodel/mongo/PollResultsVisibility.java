/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.theintelligentbook.ibmodel.mongo;

/**
 *
 * @author william
 */
public enum PollResultsVisibility {
    visible,
    afterVote,
    secret
}
