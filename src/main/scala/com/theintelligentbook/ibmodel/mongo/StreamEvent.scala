package com.theintelligentbook.ibmodel.mongo

import com.mongodb.casbah.Imports._
import java.util.Date
import com.novus.salat._
import com.novus.salat.global._
import com.novus.salat.dao.SalatDAO
import com.wbillingsley.handy.{RefNone, RefItself, RefById, Ref}
import com.novus.salat.annotations._


case class StreamEvent(

  _id:ObjectId = new ObjectId(),

  @Key("ce") _ce:Option[ObjectId] = None,

  @Key("book") _book:Option[ObjectId] = None,

  @Key("reader") _reader:Option[ObjectId] = None,

  session:Option[String] = None,

  action:String,  
 
  time:Date = new Date

) extends HasObjectId  {

  def id = _id

  def book = Ref.fromOptionId(classOf[Book], _book)

  def ce = Ref.fromOptionId(classOf[ContentEntry], _ce)

  def reader = Ref.fromOptionId(classOf[Reader], _reader)

}


object StreamEventDAO extends AbstractDAO[StreamEvent]("streamEvent") {

  def addStreamEvent(
    ce:Ref[ContentEntry] = RefNone,
    book:Ref[Book] = RefNone,
    reader:Ref[Reader] = RefNone,
    session:Option[String] = None,
    action:String
  ) = {
    val e = new StreamEvent(
      _book = book.getId, _ce = ce.getId, _reader = reader.getId,
      session=session, action=action
    )
    DAO.cache(RefById(classOf[StreamEvent], e.id), RefItself(e))
    sdao.save(e)
    e
  }

  /* TODO: This really isn't needed - StreamEvents are immutable. */
  def save(e:StreamEvent) {
    DAO.cache(RefById(classOf[StreamEvent], e.id), RefItself(e))
    sdao.save(e)    
  }
  
}
