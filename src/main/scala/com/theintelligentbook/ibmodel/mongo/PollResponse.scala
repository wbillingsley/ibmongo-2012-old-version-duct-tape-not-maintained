package com.theintelligentbook.ibmodel.mongo

import com.wbillingsley.handy._
import com.mongodb.casbah.Imports._
import com.novus.salat._
import com.novus.salat.global._
import com.novus.salat.dao.SalatDAO
import java.util.Date
import com.novus.salat.annotations._

case class PollResponse(
  _id:ObjectId = new ObjectId(),

  @Key("book") _book:Option[ObjectId] = None,

  @Key("poll") _poll:Option[ObjectId] = None,

  @Key("reader") var _reader:Option[ObjectId] = None,

  var session:Option[String] = None,

  var answer:Set[Int] = Set.empty[Int],

  created:Date = new Date(),

  var lastUpdated:Date = new Date()
) extends HasObjectId {

  def id = _id

  def book = Ref.fromOptionId(classOf[Book], _book)

  def poll = Ref.fromOptionId(classOf[Poll], _poll)

  def reader = Ref.fromOptionId(classOf[Reader], _reader)
  def reader_=(r:Ref[Reader]) = _reader_=(r.getId)

}

object PollResponseDAO extends AbstractDAO[PollResponse]("pollResponse") {

  def newPollResponse(p:Ref[Poll], r:Ref[Reader], s:Option[String], ans:Set[Int]) = {
    val pr = new PollResponse(
      _poll = p.getId,
      _book = p.flatMap(_.book).getId,
      _reader = r.getId,
      session = s,
      answer = ans
    )
    DAO.cache(RefById(classOf[PollResponse], pr.id), RefItself(pr))
    pr
  }

  def save(p:PollResponse) {
    DAO.cache(RefById(classOf[PollResponse], p.id), RefItself(p))
    sdao.save(p)
  }

  def findVote(pollRef: Ref[Poll], readerRef: Ref[Reader], sessionOpt: Option[String]) = {
    readerRef.fetch match {
      case RefItself(r) => {
        val ccOpt = sdao.findOne(MongoDBObject("poll" -> pollRef.getId, "reader" -> r.id))
        ccOpt
      }
      case _ => {
        val ccOpt = sdao.findOne(MongoDBObject("poll" -> pollRef.getId, "session" -> sessionOpt))
        ccOpt
      }
    }
  }

  def getPollResponses(pollRef:Ref[Poll]) = {
    val res = sdao.find(MongoDBObject("poll" -> pollRef.getId))
    res.toSeq
  }

}