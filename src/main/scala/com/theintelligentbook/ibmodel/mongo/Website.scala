package com.theintelligentbook.ibmodel.mongo

import com.mongodb.casbah.Imports._
import com.novus.salat._
import com.novus.salat.global._
import com.novus.salat.dao.SalatDAO
import com.wbillingsley.handy.{RefItself, RefNone, RefById, Ref}
import com.novus.salat.annotations._

/**
 * Represents a website (an external URL)
 * @param cc
 */
case class Website(
  _id:ObjectId = new ObjectId(),

  @Key("ce") _ce:Option[ObjectId] = None,

  @Key("book") _book:Option[ObjectId] = None,

  var url:Option[String] = None,

  var noFrame:Boolean = false
) extends HasObjectId {

  def id = _id

  def ce = Ref.fromOptionId(classOf[ContentEntry], _ce)

  def book = Ref.fromOptionId(classOf[Book], _book)

}

object WebsiteDAO extends AbstractDAO[Website]("website") {

  def newWebsite(book:Ref[Book], ce:Ref[ContentEntry], url:Option[String], noFrame:Boolean = false) = {
    val w = new Website(
      _book = book.getId,
      _ce=ce.getId,
      url=url, noFrame=noFrame
    )
    DAO.cache(RefById(classOf[Website], w.id), RefItself(w))
    w
  }

  def save(p:Website) {
    DAO.cache(RefById(classOf[Website], p.id), RefItself(p))
    ContentEntryDAO.touch(p.ce)
    sdao.save(p)
  }

}

