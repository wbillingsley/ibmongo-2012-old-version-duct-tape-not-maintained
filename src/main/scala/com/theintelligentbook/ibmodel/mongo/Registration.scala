package com.theintelligentbook.ibmodel.mongo

import com.wbillingsley.handy._
import com.mongodb.casbah.Imports._
import java.util.Date
import com.novus.salat._
import com.novus.salat.global._
import com.novus.salat.dao.SalatDAO
import com.novus.salat.annotations._

case class Registration(

  var _id:ObjectId = new ObjectId(),

  @Key("roles") var _roles:Set[String] = Set(BookRole.Reader.toString),

  @Key("reader") var _reader:Option[ObjectId] = None,

  @Key("book") var _book:Option[ObjectId] = None

) extends HasObjectId {

  def id = _id

  def roles = _roles.map {BookRole.valueOf(_)}
  def roles_=(s:Set[BookRole]) = _roles_=(s.map(_.toString))
  def addRole(r:BookRole) {
    _roles += r.toString
  }

  def reader = Ref.fromOptionId(classOf[Reader], _reader)
  def reader_=(r:Ref[Reader]) = _reader_=(r.getId)

  def book = Ref.fromOptionId(classOf[Book], _book)
  def book_=(r:Ref[Book]) = _book_=(r.getId)

}

object RegistrationDAO extends AbstractDAO[Registration]("registration") {

  def newRegistration(b:Ref[Book], r:Ref[Reader], roles:Set[BookRole]) = {
    val reg = new Registration
    reg.reader = r
    reg.book = b
    reg.roles = roles
    DAO.cache(RefById(classOf[Registration], reg.id), RefItself(reg))
    reg
  }

  def save(r:Registration) {
    DAO.cache(RefById(classOf[Registration], r.id), RefItself(r))
    sdao.save(r)
  }

  def findByReader(rid:ObjectId) = {
    val cursor = sdao.find(MongoDBObject("reader" -> rid))
    cursor.toSeq
  }

  def findByReaderAndBook(r:Ref[Reader], b:Ref[Book]):TraversableOnce[Registration] = {
    (for (rid <- r.getId; bid <- b.getId) yield {
      sdao.find(MongoDBObject("reader" -> rid, "book" -> bid))
    }) getOrElse Seq.empty[Registration]
  }

}
