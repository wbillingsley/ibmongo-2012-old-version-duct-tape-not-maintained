package com.theintelligentbook.ibmodel.mongo

import Imports._
import com.wbillingsley.handy._
import com.mongodb.casbah.Imports._
import com.novus.salat._
import com.novus.salat.global._
import com.novus.salat.dao.SalatDAO
import java.util.Date
import com.novus.salat.annotations._

case class Vote(
    
  @Key("addedBy") _addedBy:Option[ObjectId] = None,
  
  date:java.util.Date = new java.util.Date(),
  
  up:Boolean = true
    
) {
  
  def addedBy = Ref.fromOptionId(classOf[Reader], _addedBy)

}