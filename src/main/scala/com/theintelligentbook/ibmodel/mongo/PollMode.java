package com.theintelligentbook.ibmodel.mongo;

public enum PollMode {
    chooseOne,
    chooseMany
}
