package com.theintelligentbook.ibmodel.mongo

import com.mongodb.casbah.Imports._
import java.util.Date
import com.novus.salat._
import com.novus.salat.global._
import com.novus.salat.dao.SalatDAO
import com.wbillingsley.handy.{RefById, RefItself, Ref, ResolvedRef}
import com.novus.salat.annotations._


/**
 * Database case class that Salat puts into MongoDB
 */
case class Organisation (

  val _id: ObjectId = new ObjectId(),

  var name: Option[String] = None,
  
  var shortDesc: Option[String] = None,

  var logoURL:Option[String] = None,

  var created:Date = new Date(),
  
  @Key("addedBy") var _addedBy:Option[ObjectId] = None

) extends HasObjectId {
  
  def id = _id
  
  /**
   * Ref to the Reader who added this Organisation
   */
  def addedBy = Ref.fromOptionId(classOf[Reader], _addedBy)

}



object OrganisationDAO extends AbstractDAO[Organisation]("organisation") {

  def newOrganisation = {
    val b = new Organisation
    DAO.cache(RefById(classOf[Book], b.id), RefItself(b))
    b
  }

  def allOrganisations:Seq[Organisation] = {
    sdao.find(MongoDBObject()).toSeq
  }

  def save(b:Organisation) {
    DAO.cache(RefById(classOf[Organisation], b.id), RefItself(b))
    sdao.save(b)
  }

}