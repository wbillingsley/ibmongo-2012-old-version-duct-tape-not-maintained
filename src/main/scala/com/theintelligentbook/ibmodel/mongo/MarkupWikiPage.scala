package com.theintelligentbook.ibmodel.mongo

import com.mongodb.casbah.Imports._
import java.util.Date
import com.novus.salat._
import com.novus.salat.global._
import com.novus.salat.dao.SalatDAO
import com.wbillingsley.handy.{RefItself, RefNone, RefById, Ref}
import com.novus.salat.annotations._

case class MarkupWikiPage(
  _id:ObjectId = new ObjectId(),

  @Key("ce") _ce:Option[ObjectId] = None,

  @Key("book") _book:Option[ObjectId] = None,

  @Key("addedBy") _addedBy:Option[ObjectId] = None,

  @Key("copiedFrom") _copiedFrom:Option[ObjectId] = None,

  var content:String = "",

  created:Date = new Date(),

  var lastUpdated:Date = new Date(),

  var version:Int = 1
) extends HasObjectId {

  def id = _id

  def ce = Ref.fromOptionId(classOf[ContentEntry], _ce)

  def book = Ref.fromOptionId(classOf[Book], _book)

  def addedBy = Ref.fromOptionId(classOf[Reader], _addedBy)

  def protect = ce.toOption.map(_.protect).getOrElse(false)

}

object MarkupWikiPageDAO extends AbstractDAO[MarkupWikiPage]("markupWikiPage") {

  def newWikiPage(book:Ref[Book], ce:Ref[ContentEntry], reader:Ref[Reader], content:String) = {
    val w = new MarkupWikiPage(
      _book = book.getId,
      _ce=ce.getId,
      _addedBy = reader.getId,
      content = content
    )
    DAO.cache(RefById(classOf[WikiPage], w.id), RefItself(w))
    w
  }

  def save(p:MarkupWikiPage) {
    DAO.cache(RefById(classOf[WikiPage], p.id), RefItself(p))
    ContentEntryDAO.touch(p.ce)
    sdao.save(p)
  }

}

