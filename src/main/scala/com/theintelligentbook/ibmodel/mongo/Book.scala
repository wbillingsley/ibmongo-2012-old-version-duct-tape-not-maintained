package com.theintelligentbook.ibmodel.mongo

import com.mongodb.casbah.Imports._
import java.util.Date
import com.novus.salat._
import com.novus.salat.global._
import com.novus.salat.dao.SalatDAO
import com.wbillingsley.handy.{RefById, RefItself, Ref, ResolvedRef}
import com.novus.salat.annotations._



case class TitleAndTag(title:String, tag:String)

/**
 * An Intelligent Book
 */
case class Book (

  var _id: ObjectId = new ObjectId(),

  var title: Option[String] = None,

  var shortName:Option[String] = None,

  @Key("shortDesc") var shortDescription:Option[String] = None,

  @Key("longDesc") var longDescription:Option[String] = None,

  var edition:Option[String] = None,

  @Key("imageURL") var coverImageURL:Option[String] = None,

  var created:Date = new Date(),

  var expired:Option[Date] = None,

  @Key("organisation") var _organisation:Option[ObjectId] = None,

  @Key("precededBy") var _precededBy:Option[ObjectId] = None,

  @Key("supercededBy") var _supercededBy:Option[ObjectId] = None,

  @Key("copyPolicy") var _copyPolicy:String = BookCopyPolicy.open.toString,

  @Key("signupPolicy") var _signupPolicy:String = BookSignupPolicy.open.toString,

  @Key("chatPolicy") var _chatPolicy:String = BookChatPolicy.allReaders.toString,
  
  var listNouns:Seq[TitleAndTag] = Seq(TitleAndTag("Lectures", "Lecture"), TitleAndTag("Tutorials", "Tutorial")),
  
  var listed:Boolean = false, 
  
  @Key("orgs") var _orgs:Set[ObjectId] = Set.empty[ObjectId]

  // TODO maybe add metadata

) extends HasObjectId {
  
  def id = _id
  
  def precededBy = Ref.fromOptionId(classOf[Book], _precededBy)
  def precededBy_=(r:Ref[Book]) {
    _precededBy = r.getId.asInstanceOf[Option[ObjectId]]
  }

  def supercededBy = Ref.fromOptionId(classOf[Book], _supercededBy)
  def supercededBy_=(r:Ref[Book]) {
    _supercededBy = r.getId.asInstanceOf[Option[ObjectId]]
  }

  /**
   * Which books may receive content from this book in a "rinse and repeat"
   */
  def copyPolicy = BookCopyPolicy.valueOf(_copyPolicy)
  def copyPolicy_=(c:BookCopyPolicy) {
    _copyPolicy = c.toString
  } 

  /**
   * Who may read this book
   */
  def signupPolicy = BookSignupPolicy.valueOf(_signupPolicy)
  def signupPolicy_=(s:BookSignupPolicy) {
    _signupPolicy = s.toString
  } 

  /**
   * Who may chat, add notes, etc
   * @return
   */
  def chatPolicy = BookChatPolicy.valueOf(_chatPolicy)
  def chatPolicy_=(s:BookChatPolicy) {
    _chatPolicy = s.toString  
  }
  
  def organisations = _orgs.map { RefById(classOf[Organisation], _) }
  def addOrganisation(o:Ref[Organisation]) {
    o.getId.foreach { id => _orgs += id }
  }
  def removeOrganisation(o:Ref[Organisation]) {
    o.getId.foreach { id => _orgs -= id }
  }
  
  def invites = BookInvitationDAO.invitesByBook(RefItself(this))  
  
}


object BookDAO extends AbstractDAO[Book]("book") {

  def newBook = {
    val b = new Book
    DAO.cache(RefById(classOf[Book], b.id), RefItself(b))
    b
  }

  def allBooks:Seq[Book] = {
    sdao.find(MongoDBObject()).toSeq
  }

  def listedBooks:Seq[Book] = {
    sdao.find(MongoDBObject("listed" -> true)).toSeq
  }

  def save(b:Book) {
    DAO.cache(RefById(classOf[Book], b.id), RefItself(b))
    sdao.save(b)
  }

}
