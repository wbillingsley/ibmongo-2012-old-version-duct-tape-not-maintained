package com.theintelligentbook.ibmodel.mongo


import com.mongodb.casbah.Imports._
import java.util.Date
import com.novus.salat._
import com.novus.salat.global._
import com.novus.salat.dao.SalatDAO

import com.wbillingsley.handy._
import com.novus.salat.annotations._

case class Identification(

  _id:ObjectId = new ObjectId(),

  @Key("owner") var _owner:Option[ObjectId] = None,

  /**
   * For example, OpenID, OAuth 1.0, OAuth 2.0, Email
   */
  var kind:String = "unknown",

  /**
   * For example, Google, Twitter, Facebook
   */
  var provider:String = "unknown",

  var value:Option[String] = None,

  var verified:Boolean = false,

  var since:Date = new Date()
) extends HasId[ObjectId] {

  def id = _id

  def owner = Ref.fromOptionId(classOf[Reader], _owner)
  def owner_=(r:Ref[Reader]) {
    _owner = r.getId.asInstanceOf[Option[ObjectId]]
  }

  var isNew = false

  def save() = {
    IdentificationDAO.save(this)
  }
}

object IdentificationDAO extends AbstractDAO[Identification]("identification") {

  /**
   * Finds an @link{Identification} based on the provider and the provider's id.
   */
  def find(kind: String, provider:String, value:String):ResolvedRef[Identification] = {
    val ccOpt = sdao.findOne(MongoDBObject("kind" -> kind, "provider" -> provider, "value" -> Some(value)))
    Ref.fromOptionItem(ccOpt)
  }

  def newIdentification(kind:String, provider:String, value:String) = {
    val i = new Identification()
    i.isNew = true
    i.kind = kind
    i.provider = provider
    i.value = Some(value)
    DAO.cache(RefById(classOf[Identification], i.id), RefItself(i))
    i
  }

  def save(i:Identification) {
    DAO.cache(RefById(classOf[Identification], i.id), RefItself(i))
    sdao.save(i)
  }

}
