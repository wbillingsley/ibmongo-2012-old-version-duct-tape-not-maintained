package com.theintelligentbook.ibmodel.mongo

import com.mongodb.casbah.Imports._
import java.util.Date
import com.novus.salat._
import com.novus.salat.global._
import com.novus.salat.dao.SalatDAO
import com.wbillingsley.handy.{RefItself, RefById, Ref}
import com.novus.salat.annotations._

case class WikiPageHistory(
  _id:ObjectId = new ObjectId(),

  @Key("book") _book:Option[ObjectId] = None,

  @Key("page") _page:Option[ObjectId] = None,

  version:Int = 1,

  @Key("addedBy") _addedBy:Option[ObjectId] = None,

  forwardDiff:Option[String] = None,

  reverseDiff:Option[String] = None,

  comment:Option[String] = None,

  lastUpdated:Date = new Date()
) extends HasObjectId {

  def id = _id

  def book = Ref.fromOptionId(classOf[Book], _book)

  def page = Ref.fromOptionId(classOf[WikiPage], _page)

  def addedBy = Ref.fromOptionId(classOf[Reader], _addedBy)
  
}

object WikiPageHistoryDAO extends AbstractDAO[WikiPageHistory]("wikiPageHistory") {

  def save(p:WikiPageHistory) {
    DAO.cache(RefById(classOf[WikiPageHistory], p.id), RefItself(p))
    sdao.save(p)
  }

  def newWikiPageHistory(wikiPageRef:Ref[WikiPage], readerRef:Ref[Reader],
                         forwardDiff:Option[String], reverseDiff:Option[String],
                         version: Int, comment:Option[String]
                          ) = {
    val p = new WikiPageHistory(
      _book = wikiPageRef.flatMap(_.book).getId,
      _page = wikiPageRef.getId,
      _addedBy = readerRef.getId,
      version = version,
      reverseDiff = reverseDiff,
      forwardDiff = forwardDiff,
      comment = comment
    )
    DAO.cache(RefById(classOf[WikiPageHistory], p.id), RefItself(p))
    p
  }
}
