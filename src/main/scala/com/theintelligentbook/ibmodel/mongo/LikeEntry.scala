package com.theintelligentbook.ibmodel.mongo

import com.mongodb.casbah.Imports._
import java.util.Date
import com.novus.salat._
import com.novus.salat.global._
import com.novus.salat.dao.SalatDAO
import com.wbillingsley.handy.{RefNone, RefItself, RefById, Ref}
import com.novus.salat.annotations._

/**
 * Created with IntelliJ IDEA.
 * User: wbillingsley
 * Date: 21/08/12
 * Time: 11:59 PM
 * To change this template use File | Settings | File Templates.
 */


case class LikeEntry(

  _id:ObjectId = new ObjectId(),

  @Key("ce") _ce:Option[ObjectId] = None,

  @Key("book") _book:Option[ObjectId] = None,

  @Key("reader") var _reader:Option[ObjectId] = None,

  var session:Option[String] = None,

  var like:Boolean = true,

  var dislike:Boolean = false,

  @Key("comment") var _comment:Option[ObjectId] = None,

  time:Date = new Date

) extends HasObjectId  {

  def id = _id

  def book = Ref.fromOptionId(classOf[Book], _book)

  def ce = Ref.fromOptionId(classOf[ContentEntry], _ce)

  def reader = Ref.fromOptionId(classOf[Reader], _reader)
  def reader_=(r:Ref[Reader]) {
    _reader = r.getId
  }

  def comment = Ref.fromOptionId(classOf[EntryComment], _comment)
  def comment_=(cRef:Ref[EntryComment]) {
    _comment = cRef.getId
  }

}


object LikeEntryDAO extends AbstractDAO[LikeEntry]("likeEntry") {

  def newLikeEntry(
    ce:Ref[ContentEntry] = RefNone,
    book:Ref[Book] = RefNone,
    reader:Ref[Reader] = RefNone,
    session:Option[String] = None,
    like:Boolean = true,
    dislike:Boolean = false,
    comment:Ref[EntryComment] = RefNone
  ):LikeEntry = {
    val e = new LikeEntry(
      _book = book.getId, _ce = ce.getId, _reader = reader.getId,
      session=session, like=like, dislike=dislike, _comment=comment.getId
    )
    DAO.cache(RefById(classOf[LikeEntry], e.id), RefItself(e))
    e
  }

  def findLikeEntry(ceRef: Ref[ContentEntry], readerRef: Ref[Reader], sessionOpt: Option[String]) = {
    readerRef.fetch match {
      case RefItself(r) => {
        val ccOpt = sdao.findOne(MongoDBObject("ce" -> ceRef.getId, "reader" -> r.id))
        ccOpt
      }
      case _ => {
        val ccOpt = sdao.findOne(MongoDBObject("ce" -> ceRef.getId, "session" -> sessionOpt))
        ccOpt
      }
    }
  }
  


  def save(e:LikeEntry) {
    DAO.cache(RefById(classOf[LikeEntry], e.id), RefItself(e))
    updateCeLikeCount(e)
    sdao.save(e)
  }
  
  def updateCeLikeCount(e:LikeEntry) {
    val ceid = e.ce.getId
    val likeCount = sdao.find(MongoDBObject("ce" -> ceid, "like" -> true)).count
    val dislikeCount = sdao.find(MongoDBObject("ce" -> ceid, "dislike" -> true)).count
    ContentEntryDAO.sdao.update(MongoDBObject("_id" -> ceid), $set (Seq("nlike" -> likeCount, "ndislike" -> dislikeCount)))
  }

}
