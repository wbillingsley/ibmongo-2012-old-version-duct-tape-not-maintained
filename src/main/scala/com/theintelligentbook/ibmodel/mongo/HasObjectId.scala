package com.theintelligentbook.ibmodel.mongo

import com.mongodb.casbah.Imports._
import com.wbillingsley.handy._

trait HasObjectId extends HasId[ObjectId] {

}

object HasObjectId {
  
  implicit object GetsObjectId extends GetsId[HasObjectId, ObjectId] {
    def getId(obj:HasObjectId) = Some(obj.id)
    
    def canonical(k:Any) = {
      Some(new ObjectId(k.toString))
    }
  } 
  
}