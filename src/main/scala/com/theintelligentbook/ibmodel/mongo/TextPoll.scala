/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.theintelligentbook.ibmodel.mongo

import com.wbillingsley.handy._
import com.mongodb.casbah.Imports._
import java.util.Date
import com.novus.salat._
import com.novus.salat.global._
import com.novus.salat.dao.SalatDAO
import com.novus.salat.annotations._

case class TextPoll(
  _id:ObjectId = new ObjectId(),

  @Key("book") _book:Option[ObjectId] = None,

  @Key("ce") _ce:Option[ObjectId] = None,

  @Key("createdBy") _createdBy:Option[ObjectId] = None,

  var question:Option[String] = None,

  var anonymous:Boolean = true,

  @Key("resultsVis") var _resultsVis:String = PollResultsVisibility.visible.toString,

  var multiVote:Boolean = true,
  
  var answers: Seq[TextPollAnswer] = Seq.empty,
  
  var wordCounts: Map[String, Int] = Map.empty,

  created : Date = new Date()
) extends HasObjectId  {

  def id = _id

  def ce = Ref.fromOptionId(classOf[ContentEntry], _ce)

  def book = Ref.fromOptionId(classOf[Book], _book)

  def createdBy = Ref.fromOptionId(classOf[Reader], _createdBy)

  def resultsVisibility = PollResultsVisibility.valueOf(_resultsVis)
  def resultsVisibility_=(v:PollResultsVisibility) = _resultsVis_=(v.toString)

  def protect = ce.toOption.map(_.protect).getOrElse(false)
  
}

case class TextPollAnswer(
  _id: ObjectId = new ObjectId(),
    
  text:String,
  
  words:Set[String],
  
  @Key("addedBy") _addedBy: Option[ObjectId],
  
  created: Date = new Date() 
)


object TextPollDAO extends AbstractDAO[TextPoll]("textPoll") {

  def newPoll(b:Ref[Book], ce:Ref[ContentEntry], r:Ref[Reader], text:String) = {
    val p = new TextPoll(
      _book = b.getId,
      _ce = ce.getId,
      _createdBy = r.getId
    )
    DAO.cache(RefById(classOf[TextPoll], p.id), RefItself(p))
    p
  }

  def save(p:TextPoll) {
    DAO.cache(RefById(classOf[Poll], p.id), RefItself(p))
    ContentEntryDAO.touch(p.ce)
    
    /*
     * To avoid overwriting the embedded answers and results, we use an upsert to 
     * set the properties rather than save the whole object
     */
    val o = MongoDBObject("$set" -> MongoDBObject(
        "book" -> p._book, "ce" -> p._ce, "createdBy" -> p._createdBy, 
        "question" -> p.question, "anonymous" -> p.anonymous, "resultsVis" -> p._resultsVis, 
        "multiVote" -> p.multiVote, "created" -> p.created
    ))
    val q = MongoDBObject("_id" -> p._id)
    sdao.update(q, o, true)
  }
  
  
  /**
   * Adds a free-text answer and its associated list of words
   */
  def addAnswer(p:Ref[TextPoll], who:Ref[Reader], text:String, words:Set[String]) = {    
    for (pId <- p.getId) yield {
      val ans = new TextPollAnswer(_addedBy = who.getId, text = text, words = words)
      val incMap = MongoDBObject(words.toSeq.map(w => ("wordCounts." + w) -> 1):_*)
      val u = MongoDBObject(
        "$push" -> MongoDBObject("answers" -> grater[TextPollAnswer].asDBObject(ans)),
        "$inc" -> incMap
      ) 
      val q = MongoDBObject("_id" -> pId)
      sdao.update(q, u) 
      ans
    }
  }

}
