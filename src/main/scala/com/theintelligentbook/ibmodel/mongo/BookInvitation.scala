package com.theintelligentbook.ibmodel.mongo

import com.mongodb.casbah.Imports._
import java.util.Date
import com.novus.salat._
import com.novus.salat.global._
import com.novus.salat.dao.SalatDAO
import com.wbillingsley.handy._
import com.novus.salat.annotations._

case class InvitationFailed(msg:String) extends Throwable(msg)

case class BookInvitation (

  var _id:ObjectId = new ObjectId(),

  @Key("book") var _book:Option[ObjectId] = None,

  var code:Option[String] = Some((new ObjectId()).toStringBabble),

  @Key("roles") var _roles:Set[String] = Set(BookRole.Reader.toString),

  @Key("createdBy") var _createdBy:Option[ObjectId] = None,

  var limitedNumber:Boolean = true,

  var number:Int = 1,

  var remaining:Int = 1,

  var created:Date = new Date(),

  var expires:Option[Date] = None,

  @Key("usedBy") var _usedBy:Seq[ObjectId] = Seq.empty[ObjectId]

) extends HasObjectId {

  def id = _id

  def book = Ref.fromOptionId(classOf[Book], _book)

  def roles = _roles.map(BookRole.valueOf(_))
  def roles_=(r:Set[BookRole]) = _roles_=(r.map(_.toString))

  def createdBy = Ref.fromOptionId(classOf[Reader], _createdBy)

  def usedBy = _usedBy.map(RefById(classOf[Reader], _))
  def addUsedBy(r:Ref[Reader]) = {
    for (id <- r.getId) {
      _usedBy = _usedBy :+ id.asInstanceOf[ObjectId]
    }
  }

}


object BookInvitationDAO extends AbstractDAO[BookInvitation]("bookInvitation") {

  def newBookInvitation(b:Ref[Book]) = {
    val inv = new BookInvitation(
      _book = b.getId
    )
    DAO.cache(RefById(classOf[BookInvitation], inv.id), RefItself(inv))
    inv
  }

  def save(b:BookInvitation) {
    DAO.cache(RefById(classOf[BookInvitation], b.id), RefItself(b))
    sdao.save(b)
  }

  def inviteByBookAndCode(bookRef: Ref[Book], code: String):ResolvedRef[BookInvitation] = {
    bookRef.getId match {
      case Some(bid) => {
        val ccOpt = sdao.findOne(MongoDBObject("book" -> bid, "code" -> code))
        Ref.fromOptionItem(ccOpt)
      }
      case _ => RefNone
    }
  }

  /**
   * Looks up all the invitations for a given book
   */
  def invitesByBook(bookRef: Ref[Book]):Seq[BookInvitation] = {
    bookRef.getId match {
      case Some(bid) => {
        val cursor = sdao.find(MongoDBObject("book" -> bid))
        cursor.toSeq
      }
      case _ => Seq.empty[BookInvitation]
    }
  }

  def useInvite(r:Reader, i:BookInvitation):ResolvedRef[Registration] = {
    val rref = RefItself(r)
    if (i.limitedNumber && i.remaining <= 0) {
      RefFailed(InvitationFailed("That invitation code has been exhausted"))
    } else if (i.expires.isDefined && i.expires.get.before(new Date())) {
      RefFailed(InvitationFailed("That invitation code has expired"))
    } else {
      if (i.limitedNumber) {
        i.remaining -= 1
      }
      i.addUsedBy(rref)
      save(i)

      val reg = RegistrationDAO.newRegistration(i.book, rref, i.roles)
      RegistrationDAO.save(reg)
      RefItself(reg)
    }

  }

}