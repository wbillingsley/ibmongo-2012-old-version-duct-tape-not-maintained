package com.theintelligentbook.ibmodel.mongo;

/**
 * Created by IntelliJ IDEA.
 * User: wbillingsley
 * Date: 28/04/11
 * Time: 9:22 PM
 * To change this template use File | Settings | File Templates.
 */
public enum BookCopyPolicy {
    open,
    ccBy,
    ccBySa,
    ccBySaNc,
    organisation,
    silver,
    gold,
    none
}
