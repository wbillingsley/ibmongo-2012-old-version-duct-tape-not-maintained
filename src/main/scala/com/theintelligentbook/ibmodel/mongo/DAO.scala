package com.theintelligentbook.ibmodel.mongo

import scala.util.DynamicVariable
import com.wbillingsley.handy._
import com.mongodb.casbah.Imports._
import scala.collection.mutable

case class LookUpFailed(msg:String) extends Exception(msg)

/**
 * Possibly slightly ill-named.  Handles starting transactions, finding objects, etc.
 */
object DAO {
  
  var database:String = "ibmongo"

  var host = "localhost"

  var port = 27017

  //var optConn:Option[MongoConnection] = None
  lazy val optConn = Some(MongoConnection(host, port))

  private val emVar:DynamicVariable[Option[CacheResolver]] = new DynamicVariable(None)

  def initConn() {
    //optConn.foreach(_.close())
    //optConn = Some(MongoConnection(host, port))
  }

  def closeConn() {
    //optConn.foreach(_.close())
    //optConn = None
  }

  def getDB() = {
    require(optConn.isDefined, "No database connection")
    optConn.get.apply(database)
  }

  def getCollection(collection:String) = {
    getDB().apply(collection)
  }

  def withEm[T](f: => T):T = {
    require(optConn.isDefined, "No database connection")
    emVar.withValue(Some(new CacheResolver())) {
      f
    }
  }

  def withTransaction[T](f: => T):T = {
    withEm(f)
  }

  def find[T](clazz: Class[T], id:Any):ResolvedRef[T] = {
    try {
      id match {
        case oid:ObjectId => {
          resolvers.get(clazz) match {
            case Some(r) => r.asInstanceOf[findById[T]].findById(oid)
            case None => RefFailed(LookUpFailed("I don't know how to look up that class")) 
          }
        }
        case _ => {
          RefFailed(LookUpFailed("I don't know how to resolve that kind of id"))
        }
      }
    } catch {
      case ex:Exception => RefFailed(ex)
    }
  }

  def cache[T](ref:UnresolvedRef[T], res:ResolvedRef[T]) = {
    emVar.value.foreach(cr => cr.cache(ref, res))
  }

  def resolve[T](unresolved: UnresolvedRef[T]) = {
    emVar.value.getOrElse(new CacheResolver()).resolve(unresolved)
  }
  
  def resolveMany[T, TT >: T](rm: RefManyById[T, _]) = {
    resolvers.get(rm.clazz) match {
      case Some(r) => {
        if (classOf[HasObjectId] isAssignableFrom rm.clazz) {
          val keys = for (
              i <- rm.rawIds; 
              c <- HasObjectId.GetsObjectId.canonical(i)
          ) yield c
          r.findManyById(keys).asInstanceOf[RefMany[T]]
        } else {
          RefFailed(LookUpFailed("I don't know how to resolve that class"))
        }        
      }
      case None => RefFailed(LookUpFailed("I don't know how to resolve that class"))
    }
  }

  private val resolvers:Map[Class[_], findById[_]] = Map(
    classOf[Book] -> BookDAO,
    classOf[BookInvitation] -> BookInvitationDAO,
    classOf[ChatComment] -> ChatCommentDAO,
    classOf[ContentEntry] -> ContentEntryDAO,
    classOf[EntryComment] -> EntryCommentDAO,
    classOf[GoogleSlides] -> GoogleSlidesDAO,
    classOf[Identification] -> IdentificationDAO,
    classOf[LikeEntry] -> LikeEntryDAO,
    classOf[MarkupWikiPage] -> MarkupWikiPageDAO,
    classOf[MediaFile] -> MediaFileDAO,
    classOf[Membership] -> MembershipDAO,
    classOf[Organisation] -> OrganisationDAO,    
    classOf[Poll] -> PollDAO,
    classOf[PollResponse] -> PollResponseDAO,
    classOf[Presentation] -> PresentationDAO,
    classOf[QnAQuestion] -> QnAQuestionDAO,
    classOf[Reader] -> ReaderDAO,
    classOf[Registration] -> RegistrationDAO,
    classOf[StreamEvent] -> StreamEventDAO,
    classOf[TextPoll] -> TextPollDAO,
    classOf[Website] -> WebsiteDAO,
    classOf[WikiPage] -> WikiPageDAO,
    classOf[WikiPageHistory] -> WikiPageHistoryDAO,
    classOf[YouTube] -> YouTubeDAO
  )

}

class CacheResolver {
  val map = mutable.Map.empty[UnresolvedRef[Any], ResolvedRef[Any]]

  def resolve[T](unresolved: UnresolvedRef[T]) = {
    unresolved match {
      case RefById(clazz, id:ObjectId) => {
        map.getOrElseUpdate(RefById(clazz, id), {
          DAO.find(clazz, id)
        }).asInstanceOf[ResolvedRef[T]]
      }
      case RefById(clazz, id:String) => {
        try {
          val oid = new ObjectId(id)
          map.getOrElseUpdate(RefById(clazz, oid), {
            DAO.find(clazz, oid)
          }).asInstanceOf[ResolvedRef[T]]
        } catch {
          case ex:IllegalArgumentException => RefNone // Malformed string Ids are usually user input
          case ex:Exception => {
            RefFailed(ex)
          }
        }
      }
      case _ => {
        // TODO: log exception
        RefFailed(LookUpFailed("Don't know how to resolve that reference"))
      }
    }
  }

  def cache[T](ref:UnresolvedRef[T], res:ResolvedRef[T]) = {
    map.put(ref, res)
  }
}


trait findById[T] { 
  
  def findById(id:ObjectId):ResolvedRef[T]

  def findManyById(ids: Seq[ObjectId]):RefMany[T]

}

abstract class AbstractDAO[T <: AnyRef](val collName:String)(implicit mot: Manifest[T]) extends findById[T] {
  
  import com.novus.salat._
  import com.novus.salat.global._
  import com.novus.salat.dao.SalatDAO
  import com.novus.salat.annotations._  
  
  lazy val sdao = new SalatDAO[T, ObjectId](DAO.getCollection(collName)) {
  }

  def findById(id:ObjectId) = {
    val ccOpt = sdao.findOneById(id = id)
    Ref.fromOptionItem(ccOpt)
  }
  
  def findManyById(ids: Seq[ObjectId]) = {
    val cursor = sdao.find(Map("_id" -> Map("$in" -> ids)))
    new RefTraversableOnce(cursor.toTraversable)
  }      
  
}
