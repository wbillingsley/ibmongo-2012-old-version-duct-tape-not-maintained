package com.theintelligentbook.ibmodel.mongo


import org.junit.Test
import org.junit.BeforeClass
import org.junit.AfterClass
import org.junit.Assert._

import com.mongodb.casbah.Imports._
import com.wbillingsley.handy.RefItself
import java.util.Date


object TestIdentification {

  @BeforeClass
  def init () = {
    DAO.database = Settings.testDBname
    DAO.initConn()
  }

  @AfterClass
  def close () = {
    DAO.getCollection(IdentificationDAO.collName).drop()
  }
}

class TestIdentification {

  @Test
  def saveAndRetrieve() = {
    var id:Option[ObjectId] = None
    DAO.withTransaction {
      val i = IdentificationDAO.newIdentification("oauth2", "twitter", "fred")
      id = Option(i.id)
      i.save()
    }

    DAO.withTransaction {
      assertEquals(
        "Couldn't retrieve identification by id",
        Some("fred"),
        IdentificationDAO.findById(id.get).toOption.flatMap(_.value)
      )
    }
  }

  @Test
  def oneIdentification() = {
    val lookfor = new Date()
    val i = IdentificationDAO.newIdentification("oauth2", "twitter", "joe")
    i.since = lookfor
    i.save()

    assertEquals(
      "Couldn't find by provider and value",
      RefItself(lookfor.toString),
      IdentificationDAO.find("oauth2", "twitter", "joe").map(_.since.toString)
    )
  }

}
