package com.theintelligentbook.ibmodel.mongo

import org.junit.{Test, After, AfterClass, Before, BeforeClass}
import org.junit.Assert._
import com.wbillingsley.handy.{Ref, RefItself}


object TestBook {

  @BeforeClass
  def init () = {
    Settings.init()    
  }

  @AfterClass
  def close () = {
    
  }
}

class TestBook {
  
  @Before
  def before = {
    DAO.getCollection(BookDAO.collName).drop()
  }

  @After
  def after = {
    DAO.getCollection(BookDAO.collName).drop()
  }
  
  @Test
  def saveAndRetrieve() {
    DAO.getCollection(BookDAO.collName).drop()
    
    val lookfor = "newBookName"
    val b = new Book()
    b.title = Some(lookfor)
    val id = b.id
    BookDAO.save(b)

    assertEquals(
      "Couldn't find saved book by id",
      Some(lookfor),
      BookDAO.findById(id).toOption.flatMap(_.title)
    )

    assertEquals(
      "Number of books in repository was wrong",
      1,
      BookDAO.allBooks.length
    )

  }


}