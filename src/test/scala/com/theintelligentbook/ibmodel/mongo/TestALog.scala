package com.theintelligentbook.ibmodel.mongo

import org.junit.{Test, AfterClass, BeforeClass, Before, After}
import org.junit.Assert._
import com.wbillingsley.handy.{Ref, RefItself, RefNone}

object TestALog {

  @BeforeClass
  def init () = {
    Settings.init()
    DAO.getCollection(BookDAO.collName).drop()
    DAO.getCollection("aggEvents").drop()
  }

}


class TestALog {
  
  var b1:Book = null
  var b2:Book = null
  
  var ce1:ContentEntry = null
  var ce2:ContentEntry = null
  
  var r1:Reader = null
  var r2:Reader = null
  
  @Before
  def before {
    b1 = new Book()
    b1.title = Some("book one")
    BookDAO.save(b1)

    b2 = new Book()
    b2.title = Some("book two")
    BookDAO.save(b2)

    ce1 = new ContentEntry()
    ce1.name = Some("entry one")
    ContentEntryDAO.save(ce1)
    
    ce2 = new ContentEntry()
    ce2.name = Some("entry two")
    ContentEntryDAO.save(ce1)
    
    r1 = new Reader()
    r1.nickname = Some("reader one")
    ReaderDAO.save(r1)
  }

  @After
  def close () = {
    //DAO.getCollection(BookDAO.collName).drop()
  }
  
  
  @Test
  def testStatsByDay {
    
    
    ALogEDAO.save(ALogEDAO.newALogE(RefItself(ce1), RefItself(b1), RefItself(r1), Some("sess1"), Some("act1"), Some("kind1"), Some("site1"), Set("adj1", "adj2"), Set("noun1", "noun2"), Set("top1", "top2"), Some("extra1")))
    ALogEDAO.save(ALogEDAO.newALogE(RefItself(ce1), RefItself(b1), RefItself(r1), Some("sess1"), Some("act1"), Some("kind1"), Some("site1"), Set("adj1", "adj2"), Set("noun1", "noun2"), Set("top1", "top2"), Some("extra1")))
    ALogEDAO.save(ALogEDAO.newALogE(RefItself(ce2), RefItself(b1), RefNone, Some("sess1"), Some("act1"), Some("kind1"), Some("site1"), Set("adj1", "adj2"), Set("noun1", "noun2"), Set("top1", "top2"), Some("extra1")))
    
    val res = ALogEDAO.updateStatsByDay(RefItself(b1))
    println(res)
    
    for (obj <- res) {
      println(obj)
    }

    println("###")
    val saved = AggEventsDAO.aggEventsJson(RefItself(b1))
    println(saved)
    for (obj <- saved) {
      println(obj)
    }    
    
  }

}