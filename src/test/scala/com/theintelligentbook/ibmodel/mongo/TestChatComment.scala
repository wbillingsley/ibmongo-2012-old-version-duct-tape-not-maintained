package com.theintelligentbook.ibmodel.mongo

import org.junit.{Test, Before, AfterClass, BeforeClass}
import com.wbillingsley.handy.{RefNone, RefItself}
import org.junit.Assert._
import scala.Some

/**
 * Created with IntelliJ IDEA.
 * User: wbillingsley
 * Date: 6/08/12
 * Time: 9:30 PM
 * To change this template use File | Settings | File Templates.
 */

object TestChatComment {
  @BeforeClass
  def init () = {
    Settings.init()
    DAO.getCollection(ChatCommentDAO.collName).drop()
  }

  @AfterClass
  def close () = {
  }
}

class TestChatComment {

  var book:Book = null

  var anAuthor:Reader = null
  var aReader:Reader = null
  var aNonReader:Reader = null

  @Before
  def setup {
    book = new Book()
    book.title = Some("TestChatComment Book")
    BookDAO.save(book)

    aReader = new Reader()
    aReader.nickname = Some("TestChatComment Reader")
    ReaderDAO.save(aReader)

    anAuthor = new Reader()
    anAuthor.nickname = Some("TestChatComment Author")
    ReaderDAO.save(anAuthor)

    RegistrationDAO.save(RegistrationDAO.newRegistration(RefItself(book), RefItself(anAuthor), Set(BookRole.Reader, BookRole.Author)))
    RegistrationDAO.save(RegistrationDAO.newRegistration(RefItself(book), RefItself(aReader), Set(BookRole.Reader)))

  }


  @Test
  def saveAndRetrieve() {

    for (i <- 1 to 100) {
      val cc = ChatCommentDAO.newChatComment(
        anonymous = false,
        book = RefItself(book),
        createdBy = RefItself(aReader),
        session = None,
        comment = Some("comment " + i),
        topics = Seq("one", "two", "three")
      )
      ChatCommentDAO.save(cc)
    }

    // Get the last 50 comments
    val last50 = ChatCommentDAO.findByBook(book=RefItself(book),
      after=RefNone, before=RefNone,
      maxElements=Some(50)
    )
    var idx = 0;
    for (i <- (51 to 100).reverse) {
      assertEquals(
      "Didn't get expected message",
      Some("comment " + i),
      last50(idx).comment
      )
      idx += 1
    }

  }

}
