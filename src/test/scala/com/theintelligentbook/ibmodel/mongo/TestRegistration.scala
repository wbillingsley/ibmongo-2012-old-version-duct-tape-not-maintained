package com.theintelligentbook.ibmodel.mongo

import org.junit.{Test, AfterClass, BeforeClass}
import org.junit.Assert._
import com.wbillingsley.handy.{Ref, RefItself}
import Ref._


object TestRegistration {

  @BeforeClass
  def init () = {
    Settings.init()
  }

  @AfterClass
  def close () = {
    DAO.getCollection(RegistrationDAO.collName).drop()
  }
}

class TestRegistration {

  @Test
  def saveAndRetrieve() {

    val b = new Book()
    val bid = b.id
    BookDAO.save(b)

    val r = new Reader()
    val rid = r.id
    ReaderDAO.save(r)

    def lookfor = Set(BookRole.Author, BookRole.Moderator)
    val reg = RegistrationDAO.newRegistration(b.itself, r.itself, lookfor)
    RegistrationDAO.save(reg)

    assertEquals(
      "Couldn't find registration I saved",
      Seq(lookfor),
      RegistrationDAO.findByReaderAndBook(r.itself, b.itself).map(_.roles).toSeq
    )
  }

  @Test
  def inviteAndRegister() {
    val b = new Book()
    val bid = b.id
    BookDAO.save(b)

    val r = new Reader()
    val rid = r.id
    ReaderDAO.save(r)

    val invite = BookInvitationDAO.newBookInvitation(b.itself)
    val lookfor = Set(BookRole.Administrator, BookRole.Moderator)
    invite.roles = lookfor
    invite.limitedNumber = true
    invite.remaining = 1
    BookInvitationDAO.save(invite)

    BookInvitationDAO.useInvite(r, invite)

    assertEquals(
      "Invitation did not add roles",
      Seq(lookfor),
      RegistrationDAO.findByReaderAndBook(r.itself, b.itself).map(_.roles).toSeq
    )

    val reader2 = new Reader()
    val rid2 = reader2.id
    ReaderDAO.save(reader2)

    BookInvitationDAO.useInvite(reader2, invite)

    assertEquals(
      "Exhausted invitation added roles",
      Seq.empty[Set[BookRole]],
      RegistrationDAO.findByReaderAndBook(reader2.itself, b.itself).map(_.roles).toSeq
    )

  }

}
