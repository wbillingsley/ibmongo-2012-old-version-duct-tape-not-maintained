package com.theintelligentbook.ibmodel.mongo


import org.junit.Assert._
import com.wbillingsley.handy.{Ref, RefItself}
import org.junit.{Before, Test, AfterClass, BeforeClass}

object TestContentEntry {

  @BeforeClass
  def init () = {
    Settings.init()
  }

  @AfterClass
  def close () = {
   // DAO.getCollection(ContentEntryDAO.collName).drop()
  }
}

class TestContentEntry {

  var book:Book = null

  var anAuthor:Reader = null
  var aReader:Reader = null
  var aNonReader:Reader = null

  @Before
  def setup {
    book = new Book()
    BookDAO.save(book)

    aReader = new Reader()
    aReader.nickname = Some("aReader")
    ReaderDAO.save(aReader)

    anAuthor = new Reader()
    anAuthor.nickname = Some("anAuthor")
    ReaderDAO.save(anAuthor)

    RegistrationDAO.save(RegistrationDAO.newRegistration(RefItself(book), RefItself(anAuthor), Set(BookRole.Reader, BookRole.Author)))
    RegistrationDAO.save(RegistrationDAO.newRegistration(RefItself(book), RefItself(aReader), Set(BookRole.Reader)))

  }


  @Test
  def saveAndRetrieve() {
    val lookfor = "newCEkind"
    val ce = ContentEntryDAO.newContentEntry(RefItself(book), RefItself(anAuthor), lookfor)
    val id = ce.id
    ContentEntryDAO.save(ce)

    assertEquals(
      "Couldn't find saved contententry by id",
      Some(lookfor),
      ContentEntryDAO.findById(id).toOption.map(_.kind)
    )
  }

  @Test
  def fetchByTopic() {
    val ce1 = ContentEntryDAO.newContentEntry(RefItself(book), RefItself(anAuthor), "one")
    val ce2 = ContentEntryDAO.newContentEntry(RefItself(book), RefItself(anAuthor), "two")
    val ce3 = ContentEntryDAO.newContentEntry(RefItself(book), RefItself(anAuthor), "three")

    ce1.topics = Set("Scrum", "Scrum roles", "Product owner")
    ce2.topics = Set("Scrum", "Scrum roles", "Scrum master")
    ce3.topics = Set("Scrum", "Burndown chart")

    ContentEntryDAO.save(ce1)
    ContentEntryDAO.save(ce2)
    ContentEntryDAO.save(ce3)


    assertArrayEquals(
      "Couldn't find entries for unique topic",
      Seq("one").toArray[Object],
      ContentEntryDAO.entriesByTopic(RefItself(book), "Product owner").map(_.kind).toArray[Object]
    )

    assertArrayEquals(
      "Couldn't find entries for two-of-three topic",
      Seq("one", "two").toArray[Object],
      ContentEntryDAO.entriesByTopic(RefItself(book), "Scrum roles").map(_.kind).toArray[Object]
    )

    assertArrayEquals(
      "Couldn't find entries for three-of-three topic",
      Seq("one", "two", "three").toArray[Object],
      ContentEntryDAO.entriesByTopic(RefItself(book), "Scrum").map(_.kind).toArray[Object]
    )

  }

}
