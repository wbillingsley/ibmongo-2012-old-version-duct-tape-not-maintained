package com.theintelligentbook.ibmodel.mongo

import org.junit.{Test, AfterClass, BeforeClass}
import org.junit.Assert._
import com.wbillingsley.handy.{Ref, RefItself, RefById}


/**
 * Created with IntelliJ IDEA.
 * User: wbillingsley
 * Date: 9/06/12
 * Time: 10:54 PM
 * To change this template use File | Settings | File Templates.
 */



object TestReader {

  @BeforeClass
  def init () = {
    RefById.lookUpMethod = new RefById.LookUp {
      def lookup[T](r: RefById[T, _]) = DAO.resolve(r)
    }
    DAO.database = Settings.testDBname
    DAO.initConn()
  }

  @AfterClass
  def close () = {
    DAO.getCollection(IdentificationDAO.collName).drop()
    DAO.getCollection(ReaderDAO.collName).drop()
  }
}


class TestReader {

  @Test
  def saveAndRetrieve() {
    val lookfor = "newReaderName"
    val r = new Reader()
    r.nickname = Some(lookfor)
    val id = r.id
    ReaderDAO.save(r)

    assertEquals(
      "Couldn't find saved reader by id",
      Some(lookfor),
      ReaderDAO.findById(id).toOption.flatMap(_.nickname)
    )
  }

  @Test
  def findByIdentification() {
    val lookfor = "identReaderName"

    val kind = "oauth2"
    val prov = "yammer"
    val value = "findByIdentification"

    val r = new Reader()
    r.nickname = Some(lookfor)
    val i = IdentificationDAO.newIdentification(kind, prov, value)
    i.owner = RefItself(r)

    i.save()
    r.save()

    val foundId = IdentificationDAO.find(kind, prov, value)
    val foundOwner = foundId.flatMap(_.owner)
    assertEquals(
      "Couldn't find reader by identification",
      Some(lookfor),
      foundOwner.toOption.flatMap(_.nickname)
    )

  }

}
