name := "ibmongo"

organization := "com.theintelligentbook"

scalaVersion := "2.10.0"

scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature")

version := "0.2-SNAPSHOT"

resolvers += "handy" at "https://bitbucket.org/wbillingsley/mavenrepo/raw/master/snapshots/"

resolvers += "repo.novus rels" at "http://repo.novus.com/releases/"

resolvers += "repo.novus snaps" at "http://repo.novus.com/snapshots/"

resolvers += "typesafe snaps" at "http://repo.typesafe.com/typesafe/snapshots/"

resolvers += "sonatype snaps" at "https://oss.sonatype.org/content/repositories/snapshots/"

scalacOptions ++= Seq("-unchecked", "-deprecation")

libraryDependencies += "com.wbillingsley" %% "handy" % "0.4-SNAPSHOT"

libraryDependencies += "com.novus" %% "salat-core" % "1.9.2-SNAPSHOT"

libraryDependencies += "org.mongodb" %% "casbah-gridfs" % "2.5.0"

libraryDependencies += "com.novocode" % "junit-interface" % "0.7" % "test"

crossScalaVersions := Seq("2.10.0")

